# -*- Mode: python; py-indent-offset: 4; indent-tabs-mode: nil; coding: utf-8; -*-

# def options(opt):
#     pass

# def configure(conf):
#     conf.check_nonfatal(header_name='stdint.h', define_name='HAVE_STDINT_H')

def build(bld):
    module = bld.create_ns3_module('l4s-evaluation', ['core', 'network', 'traffic-control', 'internet'])
    module.source = [
        'model/tcp-cubic.cc',
        ]

    module_test = bld.create_ns3_module_test_library('l4s-evaluation')
    module_test.source = [
        ]

    headers = bld(features='ns3header')
    headers.module = 'l4s-evaluation'
    headers.source = [
        'model/tcp-cubic.h',
        ]

    if bld.env.ENABLE_EXAMPLES:
        bld.recurse('examples')

    # bld.ns3_python_bindings()

